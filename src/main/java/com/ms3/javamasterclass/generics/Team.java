package com.ms3.javamasterclass.generics;

import java.util.ArrayList;

public class Team<T extends Player> implements Comparable<Team<T>> {
	private String name;
	private int matches;
	private int wins;
	private int loss;
	private int draw;
	
	private ArrayList<T> members = new ArrayList<T>();
	
	public Team(String name) {
		this.name = name;
		this.matches = 0;
		this.wins = 0;
		this.draw = 0;
		this.loss = 0;
	}

	public String getName() {
		return name;
	}
	
	public int getMatches() {
		return matches;
	}

	public int getWins() {
		return wins;
	}

	public int getLoss() {
		return loss;
	}

	public int getDraw() {
		return draw;
	}
	
	public boolean addPlayer(T player) {
		if(!this.members.contains(player)) {
			this.members.add(player);
			System.out.println(player.getName() + " is added to the team " + this.getName());
			return true;
		}else {
			System.out.println(player.getName() + " already on the team " + this.getName());
			return false;
		}
	}
	
	public void matchResult(Team<T> opponent, int home, int away) {
		if(home > away) {
			this.wins++;
		}else if(home == away) {
			this.draw++;
		}else {
			this.loss++;
		}
		this.matches++;
		
		if(opponent != null) {
			matchResult(null, away, home);
		}
		
	}
	
	@Override
	public int compareTo(Team<T> team) {
		if(this.getRankPoint() > team.getRankPoint()) {
			return -1;
		}else if(this.getRankPoint() < team.getRankPoint()) {
			return 1;
		}else {
			return 0;			
		}
	}

	public int getRankPoint() {
		return (this.wins*2) + this.draw; 
	};
}
