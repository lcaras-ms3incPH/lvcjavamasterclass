package com.ms3.javamasterclass.generics;

public class Main {
	public static void main(String[] args) {
		BasketballPlayer la = new BasketballPlayer("LA");
		BasketballPlayer japith = new BasketballPlayer("Japith");
		BasketballPlayer buea = new BasketballPlayer("Buea");
		BasketballPlayer chiuo = new BasketballPlayer("chiuo");
		
		Team<BasketballPlayer> barangay = new Team<BasketballPlayer>("Ginebra");
		Team<BasketballPlayer> ros = new Team<BasketballPlayer>("Rain Or Shine");
		
		barangay.addPlayer(la);
		barangay.addPlayer(japith);
		
		ros.addPlayer(buea);
		ros.addPlayer(chiuo);
		
		League<Team<BasketballPlayer>> pba = new League<Team<BasketballPlayer>>("PBA");
		
		pba.addTeam(ros);
		pba.addTeam(barangay);
		pba.printTeams();
	}

}
