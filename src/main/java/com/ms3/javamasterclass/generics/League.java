package com.ms3.javamasterclass.generics;

import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team> {
	private String name;

	public League(String name) {
		this.name = name;
	}
	
	ArrayList<T> teams = new ArrayList<T>();
	
	public boolean addTeam(T team) {
		if(!this.teams.contains(team)) {
			this.teams.add(team);
			System.out.println(team.getName() + " is added.");
			return true;
		}else {
			System.out.println(team.getName() + " is already on league.");
			return false;			
		}
	}
	
	public void printTeams() {
		Collections.sort(this.teams);
		System.out.println(this.name + " League list.");
		System.out.println("Team \tRanking");
		for(T team: this.teams) {
			System.out.println(team.getName().toUpperCase() + " :\t" + team.getRankPoint());
		}
	}
}
