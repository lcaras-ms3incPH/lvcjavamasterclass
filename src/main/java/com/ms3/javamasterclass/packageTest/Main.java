package com.ms3.javamasterclass.packageTest;

import java.util.ListIterator;

import com.ms3.javamasterclass.packageChallenge.Series;

public class Main {
	public static void main(String[] args) {
		System.out.println("nSum() Test: ");
		ListIterator<Integer> nSumList = Series.nSum(10).listIterator();
		
		while(nSumList.hasNext()) {
			System.out.print(nSumList.next().intValue() + " ");
		}
		
		System.out.println("\n\nfactorial() Test: ");
		ListIterator<Integer> factorialList = Series.factorial(10).listIterator();
		
		while(factorialList.hasNext()) {
			System.out.print(factorialList.next().intValue() + " ");
		}

		System.out.println("\n\nfibonaccii() Test: ");
		ListIterator<Integer> fibonacciList = Series.fibonacci(10).listIterator();
		
		while(fibonacciList.hasNext()) {
			System.out.print(fibonacciList.next().intValue() + " ");
		}
	}
}