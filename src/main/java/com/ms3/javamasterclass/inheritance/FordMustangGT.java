package com.ms3.javamasterclass.inheritance;

public class FordMustangGT extends Car {
	public FordMustangGT(boolean isManual) {
		super("Ford Mustang GT", "Sports Cars", 2, 2, 8, isManual, 155, 460, 0.6);
	}
	
	public void manualShift(boolean clutched, int gear) {
		if(clutched && isManual()) {
			shiftGear(gear);
			System.out.println("The rate is: " + getRate(getGear()));
			System.out.println("The on gear: " + getGear());
		}
	}
	
	public int getRate(int gear) { // the lower the gear, the higher the rate.
		int rate;
		switch(gear) {
		case 1:
			rate = 20;
			break;
		case 2:
			rate = 18;
			break;
		case 3:
			rate = 15;
			break;
		case 4:
			rate = 13;
			break;
		case 5:
			rate = 10;
			break;
		case 6:
			rate = 8;
			break;
		case 7:
			rate = 5;
			break;
		case 8:
			rate = 3;
			break;
		default:
			rate = 0;
			break;
		}
		return rate;
	}
	
	public void drive(int speed) {
		if(!isManual()) {
			autoTransmission(speed);
			accelerate(getRate(getGear()));
			System.out.println("Rate is: " + getSpeed());
			System.out.println("Gear is: " + getGear());
		}
	}
	
	public void autoTransmission(int speed) {
		if(speed <= 155 && speed > 125) {
			shiftGear(8);			
		} else if(speed <= 125 && speed > 100) {
			shiftGear(7);
		} else if(speed <= 100 && speed > 80) {
			shiftGear(5);
		} else if(speed <= 80 && speed > 60) {
			shiftGear(4);
		} else if(speed <= 60 && speed > 40) {
			shiftGear(3);
		} else if(speed <= 40 && speed > 20) {
			shiftGear(2);
		} else if(speed <= 20 && speed > 0) {
			shiftGear(1);
		} else {
			shiftGear(0);
		}
	}
}
