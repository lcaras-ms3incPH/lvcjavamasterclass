package com.ms3.javamasterclass.inheritance;

public class Main {

	public static void main(String[] args) {
		
		FordMustangGT manualMustang = new FordMustangGT(true); // instantiate a manual Ford Mustang GT
		manualMustang.showSpecs();
		System.out.println("======");
		manualMustang.startEngine();
		manualMustang.manualShift(true, 1);
		manualMustang.accelerate(manualMustang.getRate(manualMustang.getGear()));
		manualMustang.manualShift(true, 2);
		manualMustang.accelerate(manualMustang.getRate(manualMustang.getGear()));
		manualMustang.decelerate();
		manualMustang.steering("right");
		manualMustang.steerStraight();
		manualMustang.stop();
		
		
		/* 
		FordMustangGT autoMustang = new FordMustangGT(false); // instantiate an automatic transmission Ford Mustang GT
		autoMustang.showSpecs();
		autoMustang.startEngine();
		autoMustang.drive(20);
		autoMustang.accelerate(autoMustang.getRate(autoMustang.getGear()));
		autoMustang.drive(autoMustang.getSpeed());
		System.out.println("Speed " + autoMustang.getSpeed() + " gear: " + autoMustang.getGear());
		*/
	}

}
