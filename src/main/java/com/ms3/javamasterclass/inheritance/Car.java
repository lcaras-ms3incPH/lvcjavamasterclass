package com.ms3.javamasterclass.inheritance;

public class Car extends Vehicle {
	private int doors; // number of doors
	private int seats; // capacity
	private int tranmission; // number of gears
	private int gear; // 1,2,3, ... ,8,and reverse
	private boolean isManual; // manual or auto transmission
	private int topSpeed; // mph, maximum speed
	private int hp; //horsepower
	private double epa; // mpg, fuel consumptions 
	private int speed; // mph, current speed
	private String direction;
	
	public Car(String name, String type, int doors, int seats, int tranmission, boolean isManual,
			int topSpeed, int hp, double epa) {
		super(name, type);
		this.doors = doors;
		this.seats = seats;
		this.tranmission = tranmission;
		this.gear = 0; // neutral
		this.isManual = isManual;
		this.topSpeed = topSpeed;
		this.hp = hp; 
		this.epa = epa;
		this.speed = 0; // not running
		this.direction = "Forward"; // default
	}
	
	public void showSpecs() {
		System.out.println("Name: " + this.getName());
		System.out.println("Type: " + this.getType());
		System.out.println("Doors: " + this.doors);
		System.out.println("Seats: " + this.seats);
		System.out.println("Number of gears: " + this.tranmission + " gears");
		System.out.println("Top Speed: " + this.topSpeed + " mph");
		System.out.println("Horsepower: " + this.hp);
		System.out.println("EPA: " + this.epa + " mpg");
	}

	public void accelerate(int rate) { 
		if(this.topSpeed >= this.speed) { // makes sure not to exceed the topspeed
			this.speed+=rate; // increase by 5
			move(this.speed, this.direction);
		} else
			System.out.println("Maximum speed is reached!");
	}
	
	public void decelerate() {
		if(this.speed > 0) { // checks if speed is 
			this.speed-=15;
			move(this.speed, this.direction);
		} else
			System.out.println("Speed is Zero");
	}
	
	public void steerStraight() {
		System.out.println(getVelocity(this.speed, "Forward"));
	}
	
	public void shiftGear(int gear) {
		this.gear = gear;
	}
	
	public void shiftUp() {
		this.gear+=1;
	}
	
	public void shiftDown() {
		this.gear-=1;
	}
	
	@Override
	public String getVelocity(int speed, String direction) {
		if(speed > 0 && !direction.equals(""))
			return "Car is moving at " + speed + " mph, " + direction;
		
		return "Car is not moving";
	}
	
	@Override
	public void stop() {
		this.speed = 0;
		System.out.println("The Car has stoped.");
	}
	
	public int getSpeed() {
		return this.speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getDoors() {
		return doors;
	}

	public int getSeats() {
		return seats;
	}

	public int getTranmission() {
		return tranmission;
	}

	public int getGear() {
		return gear;
	}

	public boolean isManual() {
		return isManual;
	}

	public int getTopSpeed() {
		return topSpeed;
	}

	public int getHp() {
		return hp;
	}

	public double getEpa() {
		return epa;
	}
	
	
}
