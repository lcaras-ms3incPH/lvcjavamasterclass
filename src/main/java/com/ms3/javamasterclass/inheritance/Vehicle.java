package com.ms3.javamasterclass.inheritance;

public class Vehicle {
	private String name;
	private String type;
	
	private boolean isSteering; // true = right and false = left
	private boolean isMoving; // true = forward and false = backward
	private boolean isEngineOn;
	private int currentSpeed; //mph
	
	public Vehicle(String name, String type) {
		this.name = name;
		this.type = type;
		this.isMoving = false;
		this.isSteering = false;
		this.currentSpeed = 0;
		this.isEngineOn = false;
	}
	
	private boolean toogle(boolean value) {
		return (value)?false:true;
	}
	
	public void startEngine() {
		if(!this.isEngineOn) {
			this.isEngineOn = this.toogle(this.isEngineOn);
			System.out.println("The engine of the vehicle starts.");
		} else {
			System.out.println("The engine of the vehicle is still running.");
		}
	}
	
	public void turnoffEngine() {
		if(this.isEngineOn) {
			this.isEngineOn = this.toogle(this.isEngineOn);
			System.out.println("The engine of the vehicle is off.");
		} else {
			System.out.println("The engine of the vehicle is not running.");
		}
	}
	
	public void steering(String direction) {
		this.isSteering = true;
		System.out.println(getVelocity(this.currentSpeed, direction));
	}
	
	public void move(int speed, String direction) {
		this.currentSpeed=speed;
		if(this.currentSpeed > 0) {
			this.isMoving = true;
			System.out.println(getVelocity(speed, direction));
		} else 
			System.out.println("Can not move.");
	}
	
	public String getVelocity(int speed, String direction) {
		if(speed > 0 && !direction.equals("")) {
			return "The Vehicle is moving " + speed + " mph, " + direction;
		}
		return "Vehicle is not moving";
	}
	
	public void stop() {
		this.currentSpeed = 0;
		System.out.println("The Vehicle is stoped!");
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public boolean isSteering() {
		return isSteering;
	}

	public boolean isMoving() {
		return isMoving;
	}

}
