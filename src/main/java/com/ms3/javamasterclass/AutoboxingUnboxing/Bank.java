package com.ms3.javamasterclass.AutoboxingUnboxing;

import java.util.ArrayList;

public class Bank {
	private String name;
	private ArrayList<Branch> branches;
	
	public Bank(String name) {
		this.name = name;
		this.branches = new ArrayList<Branch>();
	}

	public String getName() {
		return this.name;
	}

	public ArrayList<Branch> getBranches() {
		return this.branches;
	}
	
	public void addBranch(String name) {
		if(!isBranch(name)) {
			this.branches.add(createBranch(name));
			message(1, "New branch is created : " + name + ".");
		}else {
			message(2, "Branch " + name.toUpperCase() + " is already exist.");
		}
	}
	
	private Branch createBranch(String name) {
		return new Branch(name);
	}
	
	private int getIndex(String name) {
		for(Branch b: this.branches) {
			if(b.getName().equalsIgnoreCase(name)) {
				return this.branches.indexOf(b);
			}
		}
		return -1;
	}
	
	public boolean isBranch(String name) {
		return (getIndex(name)>-1)?true:false;
	}

	public void deposit(String branchName, String customer, double amount) {
		if(this.isBranch(branchName) && this.branches.get(getIndex(branchName)).isCustomer(customer)) {
			this.branches.get(getIndex(branchName)).deposit(customer, amount);
		}else {
			message(2, "No match found.");
		}
	}
	
	public void withdraw(String branchName, String customer, double amount) {
		if(this.isBranch(branchName) && this.branches.get(getIndex(branchName)).isCustomer(customer)) {
			Branch branch = this.branches.get(getIndex(branchName));
			if(branch.getBalance(branch.getCustomers().get(branch.getIndex(customer)).getTransaction()) >= amount) {
				branch.withdraw(customer, amount);				
			}else {
				message(2, "Not enough balance.");
			}
		}else {
			message(2, "No match found.");
		}
	}
	
	public void addCustomer(String branchName, String customer, double initial) {
		if(this.isBranch(branchName)) {
			this.branches.get(getIndex(branchName)).addCustomer(customer, initial);
		}else {
			message(2, "No branch found.");
		}
	}
	
	public void printCustomerTransaction(String branchName, String customer) {
		if(this.isBranch(branchName) && this.branches.get(getIndex(branchName)).isCustomer(customer)) {
			this.branches.get(getIndex(branchName)).printTransactions(this.branches.get(getIndex(branchName)).getCustomers().get(this.branches.get(getIndex(branchName)).getIndex(customer)).getTransaction());
		}else {
			message(2, "No record found.");
		}
	}
	
	public void printBranches() {
		if(!this.branches.isEmpty()) {
			System.out.println("\n=========================\nBranch\n=========================\n");
			for(Branch b: this.branches) {
				System.out.println("Branch: " + b.getName());
				b.printCustomerList(b.getCustomers());
			}
		}else {
			System.out.println("No branch found.");
		}
	}

	private void message(int type, String msg) {
		String msgType = "";
		
		switch(type) {
		case 1:
			msgType = "Information: ";
			break;
		case 2:
			msgType = "Warning: ";
			break;
		case 3:
			msgType = "Input: ";
			break;
		}
		
		System.out.println(msgType + msg);
	}
}
