package com.ms3.javamasterclass.AutoboxingUnboxing;

import java.util.ArrayList;

public class Branch {
	private String name;
	private ArrayList<Customer> costumers;
	
	public Branch(String name) {
		this.name = name;
		this.costumers = new ArrayList<Customer>();
	}

	public String getName() {
		return name;
	}

	public ArrayList<Customer> getCustomers() {
		return costumers;
	}
	
	public double getBalance(ArrayList<Double> transaction) {
		double total = 0.0;
		for(Double t: transaction) {
			total+=t.doubleValue();
		}
		return total;
	}
	
	public void printCustomerList(ArrayList<Customer> customer) {
		if(!customer.isEmpty()) {
			for(Customer c: customer) {
				System.out.println("=================================\n" + c.getName());
				printTransactions(c.getTransaction());
				System.out.println("=================================");
			}
		}else {
			message(1, "No customers found.");
		}
	}
	
	public void printTransactions(ArrayList<Double> transaction) {
		double total = 0.0;
		System.out.println(
				"---------------------------------\n"
				+ "Transactions\n"
				+ "---------------------------------");
		System.out.println("Deposit \t\t Withdraw");
		for(Double t: transaction) {
			if(t.doubleValue()>0) {
				System.out.println("\t" + t.doubleValue());
			}else {
				System.out.println("\t\t" + t.doubleValue());
			}
			total+=t.doubleValue();
		}
		System.out.println(
				"\n---------------------------------"
				+ "\nCurrent Balance: " + total);
	}

	public void deposit(String name, double amount) {
		if(isCustomer(name)) {
			this.costumers.get(getIndex(name)).updateTransaction(amount);
			message(1, "Transaction success. " + name.toUpperCase() + " Deposit amount: " + amount);
		}else {
			message(2, "Transaction failed. " + name.toUpperCase() + " not found.");
		}
	}
	
	public void withdraw(String name, double amount) {
		if(isCustomer(name)) {
			this.costumers.get(getIndex(name)).updateTransaction(amount*-1);
			message(1, "Transaction success. " + name.toUpperCase() + " Withdraw amount: " + amount);
		}else {
			message(2, "Transaction failed. " + name.toUpperCase() + " not found.");
		}
	}
	
	public void addCustomer(String name, double initial) {
		if(!isCustomer(name)) {
			this.costumers.add(createCostumer(name, initial));
			message(1, "New customer is added to branch " + this.name + ".");
		}else {
			message(2, "Customer " + name.toUpperCase() + " is already exist in " + this.name.toUpperCase() + " branch");
		}
	}
	
	private Customer createCostumer(String name, double initial) {
		return new Customer(name, Double.valueOf(initial));
	}
	
	public int getIndex(String name) {
		for(Customer c:this.costumers) {
			if(c.getName().equalsIgnoreCase(name)) {
				return this.costumers.indexOf(c);
			}
		}
		return -1;
	}
	
	public boolean isCustomer(String name) {
		return (getIndex(name)>-1)?true:false;
	}
	
	private void message(int type, String msg) {
		String msgType = "";
		
		switch(type) {
		case 1:
			msgType = "Information: ";
			break;
		case 2:
			msgType = "Warning: ";
			break;
		case 3:
			msgType = "Input: ";
			break;
		}
		
		System.out.println(msgType + msg);
	}
}
