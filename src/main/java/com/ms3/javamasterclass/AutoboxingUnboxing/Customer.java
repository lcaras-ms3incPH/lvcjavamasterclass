package com.ms3.javamasterclass.AutoboxingUnboxing;

import java.util.ArrayList;

public class Customer {
	private String name;
	private ArrayList<Double> transaction = new ArrayList<Double>();
	
	public Customer(String name, Double initial) {
		this.name = name;
		this.transaction.add(initial);
	}

	public String getName() {
		return name;
	}

	public ArrayList<Double> getTransaction() {
		return transaction;
	}
	
	public void updateTransaction(double amount) {
		this.transaction.add(Double.valueOf(amount));
	}
}
