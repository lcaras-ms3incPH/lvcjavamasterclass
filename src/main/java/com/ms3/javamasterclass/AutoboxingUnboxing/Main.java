package com.ms3.javamasterclass.AutoboxingUnboxing;

import java.util.Scanner;

public class Main {
	private static Scanner input = new Scanner(System.in);
	private static Bank bank = new Bank("eL Bank");
	
	public static void main(String[] args) {
		boolean quit = false;
		int select = 0;
		
		while(!quit) {
			printMenu();
			System.out.print("Select an option: ");
			select = input.nextInt();
			input.nextLine();
			
			switch(select) {
			case 1:
				createBranch();
				break;
			case 2:
				createCustomer();
				break;
			case 3:
				withdraw();
				break;
			case 4:
				deposit();
				break;
			case 5:
				showBranch();
				break;
			case 6:
				showTransactions();
				break;
			case 7:
				msg(2, "Thank you!");
				quit = true;
				break;
			}
		}
	}
	
	public static void printMenu() {
		System.out.println("+++++++++++++++++++++++++++++++");
		System.out.println("Bank:" + bank.getName());
		System.out.println("\t[1] Add Branch");
		System.out.println("\t[2] Add Customer");
		System.out.println("\t[3] Withdraw");
		System.out.println("\t[4] Deposit");
		System.out.println("\t[5] Show Branch");
		System.out.println("\t[6] Show Transactions");
		System.out.println("\t[7] quit");
		System.out.println("+++++++++++++++++++++++++++++++");
	}
	
	public static void createBranch() {
		System.out.print("Enter a branch name: ");
		bank.addBranch(input.nextLine());
	}
	
	public static void createCustomer() {
		System.out.println("Enter a branch name >> customer name >> initial balance: ");
		bank.addCustomer(input.nextLine(), input.nextLine(), input.nextDouble());
	}

	public static void withdraw() {
		System.out.println("Withdraw\nEnter a branch name >> customer name >> amount: ");
		bank.withdraw(input.nextLine(), input.nextLine(), input.nextDouble());
	}

	public static void deposit() {
		System.out.println("Deposit/nEnter a branch name >> customer name >> amount: ");
		bank.deposit(input.nextLine(), input.nextLine(), input.nextDouble());
	}
	
	public static void showTransactions() {
		System.out.println("Deposit/nEnter a branch name >> customer name :");
		bank.printCustomerTransaction(input.nextLine(), input.nextLine());
	}

	public static void showBranch() {
		bank.printBranches();
	}
	
	public static void msg(int type, String message) {
		String msgType = "";
		switch(type) {
		case 1:
			msgType = "Information! :";
			break;
		case 2:
			msgType = "Warning! :";
			break;
		case 3:
			msgType = "Input :";
			break;
		default:
			msgType = "";
		}
		
		System.out.println(msgType + message);
	}
}
