package com.ms3.javamasterclass.lambda;

import java.util.function.Supplier;

public class ChallengeNumber6 {

	public static void main(String[] args) {
		Supplier<String> iLoveJava = () -> "I Love Java!";
	}
}
