package com.ms3.javamasterclass.lambda;

import java.util.function.Function;

public class ChallengeNumber3 {

	public static void main(String[] args) {
		
		//Lambda version of method everySecondChar(String).
		Function<String, String> lambdaFunction = source -> {
			StringBuilder returnVal = new StringBuilder();
			for(int i=0; i<source.length(); i++) {
				if(i%2 == 1) {
					returnVal.append(source.charAt(i));
				}
			}
			return returnVal.toString();
		};
		//Run the function
		System.out.println(lambdaFunction.apply("1234567890"));
	}
	
	public static String everySecondChar(String source) {
		StringBuilder returnVal = new StringBuilder();
		for(int i=0; i<source.length(); i++) {
			if(i%2 == 1) {
				returnVal.append(source.charAt(i));
			}
		}
		return returnVal.toString();
	}
}
