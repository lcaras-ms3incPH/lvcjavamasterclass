package com.ms3.javamasterclass.lambda;

public class ChallengeNumber1 {
	public static void main(String[] args) {
//		Runnable runnable = new Runnable() {
//			@Override
//			public void run() {
//				String myString = "Let's split this up into an array";
//				String[] parts = myString.split(" ");
//				for(String part: parts) {
//					System.out.println(part);
//				}
//			}
//		};
		
		// Lambda version
		Runnable runnable = () -> {
			String myString = "Let's split this up into an array";
			String[] parts = myString.split(" ");
			for(String part: parts) {
				System.out.println(part);
			}
		};
		
	}

}
