package com.ms3.javamasterclass.lambda;

import java.util.function.Supplier;

public class ChallengeNumber6And7 {

	public static void main(String[] args) {
		Supplier<String> iLoveJava = () -> "I Love Java!";
		String supplierResult = iLoveJava.get();
		
		System.out.println(supplierResult);
//		System.out.println(iLoveJava.get());
	}
}
