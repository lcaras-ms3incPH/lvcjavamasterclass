package com.ms3.javamasterclass.lambda;

import java.util.function.Function;

public class ChallengeNumber5 {

	public static void main(String[] args) {
		
		//Lambda version of method everySecondChar(String).
		Function<String, String> lambdaFunction = source -> {
			StringBuilder returnVal = new StringBuilder();
			for(int i=0; i<source.length(); i++) {
				if(i%2 == 1) {
					returnVal.append(source.charAt(i));
				}
			}
			return returnVal.toString();
		};
		//Run the function
		System.out.println(lambdaFunction.apply("1234567890"));
		
		//Run the method everySecondCharacter(Function<String, String>, String)
		System.out.println(everySecondCharacter(lambdaFunction, "1234567890"));
	}
	//Method that accepts function and string
	public static String everySecondCharacter(Function<String, String> lambdaFunc, String source) {
		return lambdaFunc.apply(source);
	}
	
	public static String everySecondChar(String source) {
		StringBuilder returnVal = new StringBuilder();
		for(int i=0; i<source.length(); i++) {
			if(i%2 == 1) {
				returnVal.append(source.charAt(i));
			}
		}
		return returnVal.toString();
	}
}
