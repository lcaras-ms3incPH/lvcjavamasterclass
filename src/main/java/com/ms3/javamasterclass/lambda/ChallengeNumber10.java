package com.ms3.javamasterclass.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChallengeNumber10 {

	public static void main(String[] args) {
		List<String> topNames2015 = Arrays.asList(
				"Amelia",
				"Olivia",
				"emily",
				"Isla",
				"Ava",
				"oliver",
				"Jack",
				"Charlie",
				"harry",
				"Jacob"
				);
		
		List<String> temp = new ArrayList<>();
		
		topNames2015.forEach(name -> {
			temp.add(name.substring(0, 1).toUpperCase() + name.substring(1));
		});
		
		temp.sort(String::compareTo);
		temp.forEach(System.out::println);
	}

}