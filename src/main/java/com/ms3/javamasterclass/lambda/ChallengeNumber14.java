package com.ms3.javamasterclass.lambda;

import java.util.Arrays;
import java.util.List;

public class ChallengeNumber14 {

	public static void main(String[] args) {
		List<String> topNames2015 = Arrays.asList(
				"Amelia",
				"Olivia",
				"emily",
				"Isla",
				"Ava",
				"oliver",
				"Jack",
				"Charlie",
				"harry",
				"Jacob"
				);
		
		topNames2015
			.stream()
			.map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
			.sorted()
			.forEach(System.out::println);
		
		long nameStartsWithA = topNames2015
				.stream()
				.map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
				.filter(name -> name.startsWith("A"))
				.count();
		System.out.println("\n--------------------");
		System.out.println("Name being with letter A: " + nameStartsWithA);
		System.out.println("\n--------------------");
		topNames2015
			.stream()
			.map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
			.sorted()
			.peek(System.out::println)
			.count();
	}

}
