package com.ms3.javamasterclass.lambda;

public class ChallengeNumber2 {

	public static void main(String[] args) {
		source -> {
			StringBuilder returnVal = new StringBuilder();
			for(int i=0; i<source.length(); i++) {
				if(i%2 == 1) {
					returnVal.append(source.charAt(i));
				}
			}
			return returnVal.toString();
		}
		
	}
	
	public static String everySecondChar(String source) {
		StringBuilder returnVal = new StringBuilder();
		for(int i=0; i<source.length(); i++) {
			if(i%2 == 1) {
				returnVal.append(source.charAt(i));
			}
		}
		return returnVal.toString();
	}

}
