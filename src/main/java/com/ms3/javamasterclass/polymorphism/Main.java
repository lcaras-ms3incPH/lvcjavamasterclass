package com.ms3.javamasterclass.polymorphism;

public class Main {

	public static void main(String[] args) {
		Car car = new Car("Generic", 6);
		car.startEngine();
		car.accelerate();
		car.brake();
		
		FordMustangGT mustang = new FordMustangGT();
		mustang.startEngine();
		mustang.accelerate();
		mustang.brake();

		MitsubishiSpyder eclipse = new MitsubishiSpyder();
		eclipse.startEngine();
		eclipse.accelerate();
		eclipse.brake();

		ChevroletCamaro camaro = new ChevroletCamaro();
		camaro.startEngine();
		camaro.accelerate();
		camaro.brake();
	}

}

class Car{
	private String brand;
	private int cylinder, wheel;
	private boolean engine;
	
	public Car(String brand, int cylinder) {
		this.brand = brand;
		this.cylinder = cylinder;
		this.engine = true;
		this.wheel = 4;
	}

	public String getBrand() {
		return brand;
	}

	public int getCylinder() {
		return cylinder;
	}
	
	public void startEngine() {
		System.out.println("Car starts.");
	}
	
	public void accelerate() {
		System.out.println("Car accelerates.");
	}
	
	public void brake() {
		System.out.println("Car brake.");
	}
}

class FordMustangGT extends Car {
	public FordMustangGT() {
		super("Ford Mustang GT", 8);
	}
	
	@Override
	public void startEngine() {
		System.out.println(getClass().getSimpleName() + " starts.");
	}
	@Override
	public void accelerate() {
		System.out.println(getClass().getSimpleName() + " accelerates.");
	}
	@Override
	public void brake() {
		System.out.println(getClass().getSimpleName() + " brake.");
	}
}

class MitsubishiSpyder extends Car {
	public MitsubishiSpyder() {
		super("Mitsubishi Spyder Eclipse", 8);
	}
	
	@Override
	public void startEngine() {
		System.out.println(getClass().getSimpleName() + " starts.");
	}
	@Override
	public void accelerate() {
		System.out.println(getClass().getSimpleName() + " accelerates.");
	}
	@Override
	public void brake() {
		System.out.println(getClass().getSimpleName() + " brake.");
	}
}

class ChevroletCamaro extends Car {
	public ChevroletCamaro() {
		super("Chevrolet Camaro ZL1", 8);
	}
	
	@Override
	public void startEngine() {
		System.out.println(getClass().getSimpleName() + " starts.");
	}
	@Override
	public void accelerate() {
		System.out.println(getClass().getSimpleName() + " accelerates.");
	}
	@Override
	public void brake() {
		System.out.println(getClass().getSimpleName() + " brake.");
	}
}