package com.ms3.javamasterclass.interfaceChallenge;

import java.util.ArrayList;
import java.util.List;

public class Jeepney implements ISaveable{
	private String model;
	private int capacity;
	private double fare;
	
	public Jeepney(String model, int capacity, double fare) {
		this.model = model;
		this.capacity = capacity;
		this.fare = fare;
	}
	
	public String getModel() {
		return model;
	}
	
	public int getCapacity() {
		return capacity;
	}

	public double getFare() {
		return fare;
	}
	
	public void setFare(int fare) {
		this.fare = fare;
	}

	public List<String> write() {
		List<String> item = new ArrayList<String>();
		item.add(this.model);
		item.add(this.capacity + "");
		item.add(this.fare + "");
		
		return item;
	}

	public void read(List<String> item) {
		if(!item.isEmpty()) {
			this.model = item.get(0);
			this.capacity = Integer.parseInt(item.get(1));
			this.fare = Double.parseDouble(item.get(2));
		}
	}

	@Override
	public String toString() {
		return "Jeepney " + this.model + "\nCapacity: " + this.capacity + " | Fare: " + this.fare;
	}
	
	
}
