package com.ms3.javamasterclass.interfaceChallenge;

import java.util.List;

public interface ISaveable {
	List<String> write();
	void read(List<String> item);
}
