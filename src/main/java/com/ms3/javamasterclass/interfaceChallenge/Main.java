package com.ms3.javamasterclass.interfaceChallenge;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static Scanner scanner;

	public static void main(String[] args) {
		Bus oleb = new Bus("Oleb", true);
		Jeepney jeep = new Jeepney("Model X", 20, 10);
		System.out.println(oleb.toString());
		save(oleb);
		
		oleb.setType(false);
		System.out.println("Changed type to " + oleb.getType() + "\n" + oleb.toString());
		
		save(oleb);
		System.out.println(oleb.toString());
//		load(oleb);
//		System.out.println(oleb.toString());
		
		
		System.out.println(
				"=======================\n"
				+ ""+jeep.toString());
		jeep.setFare(8);
		save(jeep);
		System.out.println(jeep.toString());
		
	}
	
	public static ArrayList<String> readValues(){
		ArrayList<String> values = new ArrayList<String>();
		
		scanner = new Scanner(System.in);
		boolean quit = false;
		int index = 0;
		
		System.out.println("Choose\n" +
							"1 to enter a string\n" +
							"0 to quit");
		
		while(!quit) {
			System.out.println("Choose an option: ");
			int choice = scanner.nextInt();
			scanner.nextLine();
			switch(choice) {
			case 0:
				quit = true;
				System.out.println("Goodbye.");
				break;
			case 1:
				System.out.println("Enter a string: ");
				String stringInput = scanner.nextLine();
				values.add(index, stringInput);
				index++;
				break;		
			}
		}
		return values;
	}
	
	public static void save(ISaveable items) {		
		for(int i=0; i<items.write().size(); i++) {
			System.out.println("Saving ... " + items.write().get(i) + ". \nPlease wait.");
		}
	}
	
	public static void load(ISaveable items) {
		List<String> values = readValues();
		items.read(values);
	}
}
