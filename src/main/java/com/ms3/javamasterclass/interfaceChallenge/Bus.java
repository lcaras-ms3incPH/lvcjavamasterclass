package com.ms3.javamasterclass.interfaceChallenge;

import java.util.ArrayList;
import java.util.List;

public class Bus implements ISaveable{
	private String name;
	private int capacity;
	private double fare;
	private boolean isDeluxe;
	
	public Bus(String name, boolean isDeluxe) {
		this.name = name;
		this.isDeluxe = isDeluxe;
		if(isDeluxe) {
			this.capacity = 30;
			this.fare = 50;
		}else {
			this.capacity = 60;
			this.fare = 30;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public boolean getType() {
		return isDeluxe;
	}

	public void setType(boolean isDeluxe) {
		this.isDeluxe = isDeluxe;
		alterState(isDeluxe);
	}
	
	private void alterState(boolean state) {
		if(state) {
			this.fare = 50;
			this.capacity = 30;
		}else {
			this.fare = 30;
			this.capacity = 60;
		}
	}

	public List<String> write() {
		List<String> item = new ArrayList<String>();
		item.add(this.name);
		item.add(this.capacity + "");
		item.add(this.fare + "");
		item.add(this.isDeluxe + "");
		
		return item;
	}

	public void read(List<String> item) {
		if(!item.isEmpty()) {
			this.name = item.get(0);
			this.capacity = Integer.parseInt(item.get(1));
			this.fare = Double.parseDouble(item.get(2));
			this.isDeluxe = Boolean.parseBoolean(item.get(3));
		}
	}

	@Override
	public String toString() {
		return "Bus " + this.name + " ("+ this.type() +") \nCapacity: " + this.capacity + " | Fare: " + this.fare;
	}
	
	private String type() {
		return (this.isDeluxe)?"Deluxe":"Ordinary";
	}

}
