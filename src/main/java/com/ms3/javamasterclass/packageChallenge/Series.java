package com.ms3.javamasterclass.packageChallenge;

import java.util.ArrayList;

public class Series {
	public static ArrayList<Integer> nSum(int range){
		ArrayList<Integer> list = new ArrayList<>();
		int current = 0;
		
		if(range>=0) {
			for(int i=0; i<=range; i++) {
				list.add(current += i);
			}
			return list;
		}else {
			return null;
		}
	}
	
	public static ArrayList<Integer> factorial(int range){
		ArrayList<Integer> list = new ArrayList<>();
		int current = 1;
		if(range>=0) {
			list.add(0);
			for(int i=1; i<=range; i++) {
				list.add(current *= i);
			}
			return list;
		}else {
			return null;
		}
	}
	
	public static ArrayList<Integer> fibonacci(int range){
		ArrayList<Integer> list = new ArrayList<>();
		
		if(range>=0) {
			for(int i = 0; i<=range; i++) {
				list.add(f(i));
			}
			
			return list;
		}else {
			return null;
		}
		
	}
	
	private static int f(int n) {
		if(n==0) {
			return 0;
		}else if(n==1) {
			return 1;
		}else {
			return f(n-1) + f(n-2);
		}
	}
}
