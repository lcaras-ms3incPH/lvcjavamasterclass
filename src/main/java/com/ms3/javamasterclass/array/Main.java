package com.ms3.javamasterclass.array;

import java.util.Scanner;

public class Main {
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("How many integers to enter? ");
		int arrayLength = input.nextInt();// default value of the lenght of array.
		
		int list[] = getIntegers(arrayLength);
		
		System.out.println("Unsorted array: ");
		printArray(list);
		
		list = sortIntegers(list); // Sort the array in descending order.
		
		System.out.println("Descending order of the array: ");
		printArray(list);
		
	}
	
	private static int[] getIntegers(int number) {
		System.out.println("Enter " + number + " integer values.\r");
		int[] values = new int[number];
		
		for(int i=0; i<values.length; i++) {
			values[i] = input.nextInt();
		}
		
		return values;
	}
	
	private static int[] sortIntegers(int[] list) {
		int temp;
		for(int i=0; i<list.length; i++) {
			for(int j=0; j<list.length; j++) {
				if(list[i] > list[j]) {
					temp = list[j];
					list[j] = list[i];
					list[i] = temp;
				}
			}
		}
		
		return list;
	}
	
	private static void printArray(int[] list) {
		for(int i=0; i<list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
	}

}
