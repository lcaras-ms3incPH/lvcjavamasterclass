package com.ms3.javamasterclass.composition;

public class Measurement {
	private double width, length, height;

	public Measurement(double width, double length, double height) {
		this.width = width;
		this.length = length;
		this.height = height;
	}
	
	public double getArea() {
		return this.width*this.length;
	}
	
	public double getDimension() {
		return this.width*this.length*this.height;
	}
}
