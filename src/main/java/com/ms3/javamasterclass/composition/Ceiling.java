package com.ms3.javamasterclass.composition;

public class Ceiling {
	private Measurement measurement;
	private String design;
	
	public Ceiling(Measurement measurement, String design) {
		this.measurement = measurement;
		this.design = design;
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public String getDesign() {
		return design;
	}
}
