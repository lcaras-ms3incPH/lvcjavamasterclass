package com.ms3.javamasterclass.composition;

public class Floor {
	private Measurement measurement;
	private String design;
	
	public Floor(Measurement measurement, String design) {
		this.measurement = measurement;
		this.design = design;
	}

	public Measurement getMeasurement() {
		return this.measurement;
	}

	public String getDesign() {
		return design;
	}
}
