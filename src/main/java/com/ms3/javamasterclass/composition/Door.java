package com.ms3.javamasterclass.composition;

public class Door {
	private Measurement measurement;
	private String color;
	private boolean isOpen;
	
	public Door(Measurement measurement, String color) {
		this.measurement = measurement;
		this.color = color;
		this.isOpen = false;
	}
	
	private boolean toogle(boolean value) {
		return (value)?false:true;
	}
	
	public void open() {
		if(!this.isOpen) {
			this.isOpen = toogle(this.isOpen);
			System.out.println("Door is open.");			
		}else {
			System.out.println("Door is already open.");
		}
	}
	
	public void close() {
		if(this.isOpen) {
			this.isOpen = toogle(this.isOpen);
			System.out.println("Door is close.");			
		}else {
			System.out.println("Door is already close.");
		}
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public String getColor() {
		return color;
	}

	public boolean isOpen() {
		return isOpen;
	}
}
