package com.ms3.javamasterclass.composition;

public class Blanket {
	private Measurement measurement;
	private String color;

	public Blanket(Measurement measurement, String color) {
		this.measurement = measurement;
		this.color = color;
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public String getColor() {
		return color;
	}
}
