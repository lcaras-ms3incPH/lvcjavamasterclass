package com.ms3.javamasterclass.composition;

public class Bed {
	private int pilow;
	private Blanket blanket;
	private Measurement measurement;
	
	public Bed(int pilow, Blanket blanket, Measurement measurement) {
		this.pilow = pilow;
		this.blanket = blanket;
		this.measurement = measurement;
	}

	public int getPilow() {
		return pilow;
	}

	public Blanket getBlanket() {
		return blanket;
	}

	public Measurement getMeasurement() {
		return measurement;
	}
}
