package com.ms3.javamasterclass.composition;

public class Room {
	private Floor floor;
	private Ceiling ceiling;
	private Wall north_wall;
	private Wall south_wall;
	private Wall east_wall;
	private Wall west_wall;
	private Door door;
	private Window window;
	private Aircon aircon;
	private Bed bed;
	
	public Room(Aircon aircon, Bed bed) {
		this.floor = new Floor(new Measurement(0.5, 15, 12), "White Tile");
		this.ceiling = new Ceiling(new Measurement(0.5, 15, 12), "Plain");
		this.north_wall = new Wall(new Measurement(0.5, 15, 12),"Blue", "North");
		this.south_wall = new Wall(new Measurement(0.5, 15, 12),"Blue", "South");
		this.east_wall = new Wall(new Measurement(0.5, 20, 12),"Blue", "East");
		this.west_wall = new Wall(new Measurement(0.5, 20, 12),"Blue", "West");
		this.door = new Door(new Measurement(0.3, 3, 8), "White");
		this.window = new Window(new Measurement(0.3, 4, 5), "White", "Sliding");
		
		this.aircon = aircon;
		this.bed = bed;
	}

	public Floor getFloor() {
		return floor;
	}

	public Wall getNorth_wall() {
		return north_wall;
	}

	public Wall getSouth_wall() {
		return south_wall;
	}

	public Wall getEast_wall() {
		return east_wall;
	}

	public Wall getWest_wall() {
		return west_wall;
	}

	public Ceiling getCeiling() {
		return ceiling;
	}

	public Door getDoor() {
		return door;
	}

	public Window getWindow() {
		return window;
	}

	public Aircon getAircon() {
		return aircon;
	}

	public Bed getBed() {
		return bed;
	}
}
