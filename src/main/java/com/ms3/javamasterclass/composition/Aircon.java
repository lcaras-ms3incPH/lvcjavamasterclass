package com.ms3.javamasterclass.composition;

public class Aircon {
	private boolean isOn;
	private boolean swingFan;
	private int temperature;
	private String brand;
	private String type;
	
	public Aircon(String brand, String type) {
		this.brand = brand;
		this.type = type;
		
		this.isOn = false;
		this.swingFan = false;
		this.temperature = 24; // default setting
	}
	
	private boolean toogle(boolean value) {
		return (value)?false:true;
	}
	
	public void power() {
		String msg = "off";
		
		if(!this.isOn) {
			msg = "on";
		}
		
		this.isOn = toogle(this.isOn);
		System.out.println("Aircon is turned-" + msg);
	}
	
	public void swingFan() {
		String msg = "not swinging";
		
		if(!this.isSwingFan()) {
			msg = "swinging";
		}
		
		this.swingFan = toogle(this.swingFan);
		System.out.println("Aircon fan is " + msg);
	}
	
	public void plusTemp() { // warmer
		if(this.temperature < 30) this.temperature++; //maximum temp 29C
	}
	
	public void minusTemp() {
		if(this.temperature > 15) this.temperature--; //minimum temp 16C
	}

	public boolean isOn() {
		return isOn;
	}

	public boolean isSwingFan() {
		return swingFan;
	}

	public int getTemperature() {
		return temperature;
	}

	public String getBrand() {
		return brand;
	}

	public String getType() {
		return type;
	}
}
