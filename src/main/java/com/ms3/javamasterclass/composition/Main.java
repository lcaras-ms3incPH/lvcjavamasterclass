package com.ms3.javamasterclass.composition;

public class Main {

	public static void main(String[] args) {
		Aircon aircon = new Aircon("Pansonic", "Window");
		Blanket blanket = new Blanket(new Measurement(0.05, 10, 8), "White");
		Bed bed = new Bed(4, blanket, new Measurement(6, 9, 2));
		Room myRoom = new Room(aircon, bed);
		
		myRoom.getDoor().open();
		myRoom.getAircon().power();
		myRoom.getDoor().close();
		myRoom.getWindow().close();
		myRoom.getWindow().open();
	}

}
