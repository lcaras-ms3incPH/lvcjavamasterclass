package com.ms3.javamasterclass.composition;

public class Wall {
	private Measurement measurement;
	private String wallpaper;
	private String position;
	
	public Wall(Measurement measurement, String wallpaper, String position) {
		this.measurement = measurement;
		this.wallpaper = wallpaper;
		this.position = position;
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public String getWallpaper() {
		return wallpaper;
	}

	public String getPosition() {
		return position;
	}
}
