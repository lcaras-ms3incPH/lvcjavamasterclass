package com.ms3.javamasterclass.innerclass;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    private SongList songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new SongList();
    }

    public boolean addSong(String title, double duration){
        return this.songs.add(new Song(title, duration));
    }

    public boolean addToPlaylist(int trackNumber, LinkedList<Song> playlist){
    	Song song = this.songs.findSong(trackNumber);
    	if(song != null) {
    		playlist.add(song);
    		return true;
    	}
    	
        System.out.println("This album does not have a track "+trackNumber);
        return false;
    }

    public boolean addToPlaylist(String title, LinkedList<Song> playlist){
        Song song = this.songs.findSong(title);
        if(song != null){
            playlist.add(song);
            return true;
        }
        System.out.println("This song "+title+" is not in this album.");
        return false;
    }
    
    private class SongList{
    	private ArrayList<Song> list;

		public SongList() {
			this.list = new ArrayList<Song>();
		}
		
		public boolean add(Song song) {
			if(!this.list.contains(song)) {
				this.list.add(song);
				return true;
			}
			return false;
		}

	    private Song findSong(String title){
	        for (Song song: this.list){
	            if(song.getTitle().equalsIgnoreCase(title)){
	                return song;
	            }
	        }
	        return null;
	    }

	    private Song findSong(int track){
	        int index = track-1;
	    	if(index > 0 && index < this.list.size()) {
	    		return this.list.get(index);
	    	}
	        return null;
	    }


    }
}