package com.ms3.javamasterclass.regularexpresschallenges;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionChalleges8To11 {

	public static void main(String[] args) {
		System.out.println("\nChallenge #8");
		String challenge8 = "abcd.135uvqz.7tzik.999";
		String pattern = "\\w\\.(\\d+)";
		regExCheck(challenge8, pattern, 1);
		
		System.out.println("\nChallenge #9");
		String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
		pattern = "\\w\\.(\\d+)(\\s)";
		regExCheck(challenge9, pattern, 1);

		System.out.println("\nChallenge #10");
		pattern = "[A-Za-z]\\.(\\d+)(\\s)";
		regExCheckIndex(challenge9, pattern);
		
		System.out.println("\nChallenge #11");
		String challenge11 = "{0, 2}, {0, 5}, {1, 3}, {2, 4}";
		pattern = "(\\{)(\\d+, \\d+)(\\})";
		regExCheck(challenge11, pattern, 2);

	}

	public static void regExCheck(String sample, String regEx, int group) {
		Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(sample);
		
		while(matcher.find()) {
			System.out.println("Occurrence : " + matcher.group(group));
		}
	}
	
	public static void regExCheckIndex(String sample, String regEx) {
		Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(sample);
		
		while(matcher.find()) {
			System.out.println("Occurrence index: " + matcher.start(1) + " to " + (matcher.end(1)-1));
		}
	}
}
