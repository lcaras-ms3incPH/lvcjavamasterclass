package com.ms3.javamasterclass.regularexpresschallenges;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionChalleges1To7 {

	public static void main(String[] args) {
		System.out.println("Challenge #1");
		String challenge1 = "I want a bike.";
		System.out.println(challenge1.matches("I want a bike."));
		
		System.out.println("\nChallenge #2");
		String challenge2 = "I want a ball.";
		System.out.println(challenge1.matches("I want a \\w+."));
		System.out.println(challenge2.matches("I want a \\w+."));
		
		System.out.println("\nChallenge #3");
		System.out.println(regExCheck(challenge1, "I want a \\w+."));
		System.out.println(regExCheck(challenge2, "I want a \\w+."));
		
		System.out.println("\nChallenge #4");
		String challenge4 = "Replace all blanks with underscores.";
		System.out.println(challenge4.replaceAll("\\s", "_"));

		System.out.println("\nChallenge #5");
		String challenge5 = "aaabccccccccdddefffg";
		System.out.println(challenge5.matches("[a-g]+"));

		System.out.println("\nChallenge #6");
		System.out.println(challenge5.matches("^a{3}bc{8}d{3}ef{3}g$"));

		System.out.println("\nChallenge #7");
		String challenge7 = "abcd.125";
		System.out.println(challenge7.matches("^\\w+\\.\\d+$"));
	}

	public static boolean regExCheck(String sample, String regEx) {
		Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(sample);
		
		return matcher.matches();
	}
}
