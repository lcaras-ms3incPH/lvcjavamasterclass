package com.ms3.javamasterclass.regularexpresschallenges;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionChalleges12To14 {

	public static void main(String[] args) {
		System.out.println("\nChallenge #12");
		String challenge12 = "11111";
		System.out.println(challenge12.matches("^\\d{5}$"));
		
		System.out.println("\nChallenge #13");
		String challenge13 = "11111-1111";
		System.out.println(challenge13.matches("^\\d{5}-\\d{4}$"));
		
		System.out.println("\nChallenge #14");
		System.out.println(challenge13.matches("^\\d{5}(-\\d{4})?$"));
		System.out.println(challenge12.matches("^\\d{5}(-\\d{4})?$"));
	}
}
