package com.ms3.javamasterclass.concurrencychallege;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Challenge9 {
	
    public static void main(String[] args) {
        NTutor tutor = new NTutor();
        NStudent student = new NStudent(tutor);
        tutor.setStudent(student);

        Thread tutorThread = new Thread(new Runnable() {
            @Override
            public void run() {
                tutor.studyTime();
            }
        });

        Thread studentThread = new Thread(new Runnable() {
            @Override
            public void run() {
                student.handInAssignment();
            }
        });

        tutorThread.start();
        studentThread.start();
    }
}

class NTutor {
    private NStudent student;
    private Lock lock;

    public void setStudent(NStudent student) {
        this.student = student;
        this.lock = new ReentrantLock();
    }

    public void studyTime() {
    	synchronized (this) {
    		System.out.println("Tutor has arrived");
//    		synchronized (student) {
			lock.lock();
			student.startStudy();
			lock.unlock();
    		try {
				this.wait();
			} catch (InterruptedException e) {
				//
			}
			System.out.println("Tutor is studying with student");
//			}
		}
    }

    public void getProgressReport() {
        // get progress report
        System.out.println("Tutor gave progress report");
    }
}

class NStudent {

    private NTutor tutor;

    NStudent(NTutor tutor) {
        this.tutor = tutor;
    }

    public void startStudy() {
        // study
        System.out.println("Student is studying");
    }

    public void handInAssignment() {
    	synchronized (tutor) {
    		tutor.getProgressReport();
    		synchronized (this) {
    			System.out.println("Student handed in assignment");
    			tutor.notifyAll();
			}
		}
    }
}