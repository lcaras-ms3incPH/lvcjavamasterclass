package com.ms3.javamasterclass.concurrencychallege;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
	private double balance;
	private String accountNumber;
	
	Lock lock;
	
	public BankAccount(String accountNumber, double balance) {
		this.balance = balance;
		this.accountNumber = accountNumber;
		this.lock = new ReentrantLock();
	}
	
	public double getBalance() {
		return balance;
	}
	
	// getAccountNumber() only read the accountNumber, no need to synchronize.
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void deposit(double amount) {
		boolean status = false; // local variable is threadsafe.
//		synchronized (this) {
//		lock.lock();
		try {
			if(lock.tryLock(1000, TimeUnit.MILLISECONDS)) {
				try {
					this.balance += amount;
					status = true;
				}finally {
					lock.unlock();			
				}				
			}else {
				System.out.println("Failed to retrieve the lock.");
			}
		}catch(InterruptedException e) {
			System.out.println("InterruptedException");
		}
//		}
		System.out.println("Diposit | transaction status: " + status);
	}
	
	public void withdraw(double amount) {
		boolean status = false; // local variable is threadsafe.
//		synchronized (this) {
//		lock.lock();
		try {
			if(lock.tryLock(1000, TimeUnit.MILLISECONDS)) {
				try {
					this.balance -= amount;
					status = true;
				}finally {
					lock.unlock();
				}
			}else {
				System.out.println("Failed to retrieve the lock.");
			}
		}catch(InterruptedException e) {
			System.out.println("InterruptedException");
		}
//		}
		System.out.println("Withdraw | transaction status: " + status);
	}
	// printAccountNumber() only read the accountNumber, no need to synchronize.
	public void printAccountNumber() {
		System.out.println("Account number : " + this.accountNumber);
	}

	@Override
	public String toString() {
		return "Account: " + this.accountNumber + "\nBalance: " + this.balance ;
	}
	
	
}
