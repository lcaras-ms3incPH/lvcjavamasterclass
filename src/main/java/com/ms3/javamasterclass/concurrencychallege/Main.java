package com.ms3.javamasterclass.concurrencychallege;

public class Main {

	public static void main(String[] args) {
		final BankAccount account = new BankAccount("01-account-001", 1000.00);
		Thread threadAccount1 = new Thread(new Runnable() {
			@Override
			public void run() {
				account.deposit(300.00);
				System.out.println("threadAccount1>> Diposit: 300.00");
				System.out.println("threadAccount1>> " + account);
				account.withdraw(50.00);
				System.out.println("threadAccount1>> Withdraw: 50.00");
				System.out.println("threadAccount1>> " + account);
			}
		});
		
		Thread threadAccount2 = new Thread(new Runnable() {
			@Override
			public void run() {
				account.deposit(203.75);
				System.out.println("threadAccount2>> Diposit: 203.75");
				System.out.println("threadAccount2>> " + account);
				account.withdraw(100.00);
				System.out.println("threadAccount2>> Withdraw: 100.00");
				System.out.println("threadAccount2>>" + account);
			}
		});
		
		threadAccount1.start();
		threadAccount2.start();
	}

}
