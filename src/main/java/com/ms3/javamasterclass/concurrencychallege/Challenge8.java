package com.ms3.javamasterclass.concurrencychallege;

public class Challenge8 {
	
    public static void main(String[] args) {
        NewTutor newTutor = new NewTutor();
        NewStudent newStudent = new NewStudent(newTutor);
        newTutor.setStudent(newStudent);

        Thread tutorThread = new Thread(new Runnable() {
            @Override
            public void run() {
                newTutor.studyTime();
            }
        });

        Thread studentThread = new Thread(new Runnable() {
            @Override
            public void run() {
                newStudent.handInAssignment();
            }
        });

        tutorThread.start();
        studentThread.start();
    }
}

class NewTutor {
    private NewStudent newStudent;

    public void setStudent(NewStudent newStudent) {
        this.newStudent = newStudent;
    }

    public void studyTime() {
        try {
            // wait for student to arrive and hand in assignment
        	System.out.println("Tutor has arrived");
            Thread.sleep(300);
        }
        catch (InterruptedException e) {

        }
        newStudent.startStudy();
        System.out.println("Tutor is studying with student");
    }

    public void getProgressReport() {
        // get progress report
        System.out.println("Tutor gave progress report");
    }
}

class NewStudent {

    private NewTutor newTutor;

    NewStudent(NewTutor newTutor) {
        this.newTutor = newTutor;
    }

    public void startStudy() {
        // study
        System.out.println("Student is studying");
    }

    public void handInAssignment() {
        newTutor.getProgressReport();
        System.out.println("Student handed in assignment");
    }
}