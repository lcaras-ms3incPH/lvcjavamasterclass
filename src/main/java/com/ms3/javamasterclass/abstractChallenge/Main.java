package com.ms3.javamasterclass.abstractChallenge;

public class Main {

	public static void main(String[] args) {
		EList list = new EList(null);
		list.traverse(list.getRoot());
		dataExtract(list);
		list.traverse(list.getRoot());
	}
	
	public static EList dataExtract(EList list) {
		String stringData = "Rosario Tomas Agoo Aringay Caba Bauang Fernando Juan Gabriel Luna Baraca";
		
		String[] data = stringData.split(" ");
		
		for(String s:data) {
			list.addItem(new Node(s));
		}
		
		return list;
	}
}
