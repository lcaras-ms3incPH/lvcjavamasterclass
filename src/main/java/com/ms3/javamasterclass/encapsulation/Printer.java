package com.ms3.javamasterclass.encapsulation;

public class Printer {
	private double tonerLevel;
	private int printed;
	private boolean isDuplex;
	
	public Printer(boolean isDuplex) {
		this.isDuplex = isDuplex;
		this.tonerLevel = 30.0; // default level is
		this.printed = 0; //
	}
	
	public void fillUp(int toner) { // The level should below 30%
		if(this.tonerLevel < 30.0) {
			double lvl = this.tonerLevel+=toner;
			
			if(lvl > 100) // Make sure the toner level do not exceed the 100
				this.tonerLevel = 100;
			
			System.out.println("Toner is filled up. Toner level is at " + this.tonerLevel + "%");
		}else {
			System.out.println("Action denied. Toner level is safe.");
		}
	}
	/**
	 * Start to simulate printing.
	 * @param pages Pages to be print
	 */
	public void print(int pages) {
		/*
		 * If duplex, 2 print-outs is equal to 1 page
		 * else 1 print-out is equal to 1 page
		 */
		int queue = rate(this.isDuplex, pages);
		double consumed = consumption(pages);
		
		if((this.tonerLevel-consumed > 0)) {
			this.printed+=queue;
			this.tonerLevel-=consumed;
			System.out.println("Finished printing: pages=> "+queue+"; toner consumption=> "+ consumed +"%; toner level=> " + this.tonerLevel);
		}else {
			System.out.println("Error printing: Low toner level.");
		}
	}
	
	/**
	 * Computes the toner consumptions.
	 * One percent (1%) of toner can print 10 pages 
	 * @param usage Pages to be print.
	 * @return Percentage (%) of consumed toner.
	 */
	private double consumption(int usage) {
		//Consumption: 1% of toner level = 10 pages
		return usage*0.1;
	}
	
	/**
	 * Checks if the printer is duplex or not.
	 * @param type Either true of false.
	 * @param pages Number of pages to be print
	 * @return Integer 2 if type is true and 1 otherwise.
	 */
	private int rate(boolean type, int pages) { 
		double rate = pages/((type)?2.0:1);
		return (int) Math.round(rate);
	}
	
	public void showSpec() {
		System.out.println( getClass().getSimpleName().toUpperCase());
		System.out.println("Type: " + ((this.isDuplex)?"Duplex":"Regular"));
		System.out.println("Toner level: " + this.tonerLevel);
	}
	
	public void status() {
		System.out.println("Total printed pages: " + this.printed);
		System.out.println("Toner level: " + this.tonerLevel);
	}

	public double getTonerLevel() {
		return tonerLevel;
	}

	public int getPrinted() {
		return printed;
	}
}
