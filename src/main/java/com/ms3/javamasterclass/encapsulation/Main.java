package com.ms3.javamasterclass.encapsulation;

public class Main {

	public static void main(String[] args) {
		Printer printer = new Printer(true);
		printer.showSpec();
		printer.print(15);
		printer.status();
		printer.fillUp(80);
	}

}
