package com.ms3.javamasterclass.accessmodifier;

/*
 * Challenge:
 * In the ff. interface declaration, what is the visibility of:
 * 
 * 1. The accessible interface?
 * 2. The int variable SOME_CONSTANT?
 * 3. methodA?
 * 4. methodB and methodC?
 */

//package-private
interface Accessible {
	//public
	int SOME_CONSTANT = 100;
	//public
	public void methodA();
	//public by default.
	void methodB();	
	//public by default.
	boolean methodC();
}
