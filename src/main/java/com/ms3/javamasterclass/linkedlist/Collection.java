package com.ms3.javamasterclass.linkedlist;

import java.util.ArrayList;

public class Collection {
	private ArrayList<Album> albums;

	public Collection() {
		this.albums = new ArrayList<Album>();
	}

	public ArrayList<Album> getAlbums() {
		return albums;
	}
	
	public int getIndexByAlbumTitle(String title) {
		if(!this.albums.isEmpty()) {
			for(Album al: this.albums) {
				if(al.getTitle().equalsIgnoreCase(title))
					return this.albums.indexOf(al);
			}
		}
		return -1;
	}
	
	public void newAlbum(String title) {
		this.albums.add(create(title));
	}
	
	private Album create(String title) {
		if(exist(title)) {
			message(2, title + " already exist.");
			return null;
		}
		message(1, title + " is created.");
		return new Album(title);
	}
	
	private boolean exist(String title) {
		for(Album al: this.albums) {
			if(al.getTitle().equalsIgnoreCase(title)) {
				return true;
			}
		}
		return false;
	}
	

	private void message(int type, String msg) {
		String msgtype = "";
		
		switch(type) {
		case 1:
			msgtype = "Information: ";
			break;
		case 2:
			msgtype = "Warning: ";
			break;
		case 3:
			msgtype = "";
			break;
		}
		
		System.out.println(msgtype + msg);
	}

}
