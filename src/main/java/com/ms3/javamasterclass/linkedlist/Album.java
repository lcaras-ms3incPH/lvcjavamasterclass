package com.ms3.javamasterclass.linkedlist;

import java.util.LinkedList;
import java.util.ListIterator;

public class Album {
	private String title;
	private LinkedList<Song> songs;
	
	public Album(String title) {
		this.title = title;
		this.songs = new LinkedList<Song>();
	}

	public String getTitle() {
		return title;
	}

	public LinkedList<Song> getSongs() {
		return songs;
	}
	
	public Song getSongByTitle(String title) {
		for(Song c: this.songs) {
			if(c.getTitle().equalsIgnoreCase(title)) {
				return c;
			}
		}
		return null;
	}
	
	public void addSong(String title, String duration) {
		ListIterator<Song> songlist = this.songs.listIterator();
		Song song = new Song(title, duration);
		
		if(exist(title)) {
			message(2, "The song " + title + "(" + duration +") is duplicated");
		}else {
			songlist.add(song);
			message(1, title + " (" + duration + ") is added.");
		}
	}
	
	public void deleteSong(String title) {
		if(getIndexByTitle(title) > -1) {
			this.songs.remove(getIndexByTitle(title));
			message(1, title + " deleted.");
		}else {
			message(2, "Operation failed. " + title + " do not exist");
		}
	}
	
	public int getIndexByTitle(String title) {
		for(Song c: this.songs) {
			if(c.getTitle().equalsIgnoreCase(title)) {
				return this.songs.indexOf(c);
			}
		}
		return -1;
	}
	
	private boolean exist(String title) {
		ListIterator<Song> songlist = this.songs.listIterator();
		while(songlist.hasNext()) {
			if(songlist.next().getTitle().compareTo(title) == 0) {
				return true;
			}
		}
		return false;
	}
	
	private void message(int type, String msg) {
		String msgtype = "";
		
		switch(type) {
		case 1:
			msgtype = "Information: ";
			break;
		case 2:
			msgtype = "Warning: ";
			break;
		case 3:
			msgtype = "";
			break;
		}
		
		System.out.println(msgtype + msg);
	}
}
