package com.ms3.javamasterclass.linkedlist;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 * @author LIMO2
 *
 */
public class Playlist {
	private String name;
	private Collection collection;
	private LinkedList<Song> playlist;
	
	public Playlist(String name, Collection collection) {
		this.name = name;
		this.collection = collection;
		this.playlist = new LinkedList<Song>();
	}

	public String getName() {
		return this.name;
	}

	public LinkedList<Song> getPlaylist() {
		return this.playlist;
	}
	
	public Collection getCollection() {
		return this.collection;
	}
	
	public void addSong(String album, String title) {
		if(!this.collection.getAlbums().isEmpty()) {
			for(Album albumlist: this.collection.getAlbums()) {
				if(albumlist.getTitle().equalsIgnoreCase(album)) {
					if(extractSong(albumlist.getSongs(), title) == null) {
						message(1, title + " is not available");
					}else {
						message(1, title.toUpperCase() + "(" + album + ") is added.");
						this.playlist.add(extractSong(albumlist.getSongs(), title));					
					}
				}else {
					message(1, album.toUpperCase() +" album not found.");
				}
			}
		}else {
			message(1, album + " not found.");
		}
	}
	
	public void printPlaylist() {
		if(!this.playlist.isEmpty()) {
			System.out.println("-------------------------------\n"+this.name+"\n-------------------------------");
			System.out.println("Title\t\tDuration");
			ListIterator<Song> list = this.playlist.listIterator();
			while(list.hasNext()) {
				Song item = list.next();
				System.out.println(item.getTitle()+"\t\t"+item.getDuration());
			}
			System.out.println("-------------------------------");
		}else {
			message(1, "Empty Playlist");
		}
	}
	
	private Song extractSong(LinkedList<Song> list, String title) {
		if(!list.isEmpty()) {
			for(Song tract: list) {
				if(tract.getTitle().equalsIgnoreCase(title)) {
					return tract;					
				}else {
					message(1, title + " is not available.");
				}
			}
		}else {
			message(1, "Empty album.");
		}
		return null;
	}
	
	private void message(int type, String msg) {
		String msgtype = "";
		
		switch(type) {
		case 1:
			msgtype = "Information: ";
			break;
		case 2:
			msgtype = "Warning: ";
			break;
		case 3:
			msgtype = "";
			break;
		}
		
		System.out.println(msgtype + msg);
	}
}
