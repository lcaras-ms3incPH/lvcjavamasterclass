package com.ms3.javamasterclass.linkedlist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class Player {
	private Collection collection;
	private Playlist playlist;
	private Scanner input;

	public Player() {
		this.collection = new Collection();
		this.playlist = new Playlist("My Playlist", this.collection);
	}

	public Collection getCollection() {
		return collection;
	}
	
	public Playlist getPlaylist() {
		return this.playlist;
	}
	
	public void runPlayer() {
		input = new Scanner(System.in);
		ListIterator<Song> songlist = this.playlist.getPlaylist().listIterator();
		boolean play = true,
				goingForward = true;
		int option = 0;
		
		while(play) {
			showMenu();
			showPlaylist();
			System.out.print("Select option:");
			option = input.nextInt();
			input.nextLine();
			
			switch(option) {
			case 1:
				play=false;
				System.out.println("Goodbye.");
				break;
			case 2:
				if(!goingForward) {
					if(songlist.hasNext()) {
						songlist.next();
					}
					goingForward = true;
				}
				if(songlist.hasNext()) {
					System.out.println("(Next) Playing " + songlist.next());
				}else {
					System.out.println("End of the list");
				}
				break;
			case 3:
				if(goingForward) {
					if(songlist.hasPrevious()) {
						songlist.previous();
					}
					goingForward = false;
				}
				if(songlist.hasPrevious()) {
					System.out.println("(Previous) Playing " + songlist.previous());
				}else {
					System.out.println("Top of the list");
				}
				break;
			case 4:
				if(songlist.hasPrevious()) {
					System.out.println("(Replay) Playing " + songlist.next());
				}
				break;
			case 5:
				System.out.println("Add song to playlist.\nEnter album name >> song title.");
				addSongToPlaylist(input.nextLine(), input.nextLine());
				break;
			case 6:
				showPlaylist();
				break;
			case 7:
				showCollection();
				break;
			case 8:
				System.out.println("Creating new album\nEnter title");
				newAlbum(input.nextLine());
				break;
			case 9:
				System.out.println("Add new song to an album\nEnter album name >> song title >> duration");
				newSong(input.nextLine(), input.nextLine(), input.nextLine());
			}
		}
	}
	
	public void showMenu() {
		System.out.println("+++++++++++++++++++++++++++++++\nMenu\n-------------------------------");
		System.out.println(
					"[1] Quit\n" +
					"[2] Next\n" +
					"[3] Previous\n" +
					"[4] Replay\n" +
					"[5] Add song to playlist\n" +
					"[6] Show playlist\n" +
					"[7] Show collections\n" +
					"[8] New album\n" +
					"[9] New song"
							);
		System.out.println("+++++++++++++++++++++++++++++++");
	}
	
	public void addSongToPlaylist(String album, String title) {
		this.playlist.addSong(album, title);
	}
	
	public void newSong(String album, String title, String duration) {
		int position = this.collection.getIndexByAlbumTitle(album);
		if(position>-1) {
			this.collection.getAlbums().get(position).addSong(title, duration);
		}else {
			message(2, album.toUpperCase() + " is not found.");
		}
	}
	
	public void newAlbum(String title) {
		this.collection.newAlbum(title);
	}
	
	public void showPlaylist() {
		this.playlist.printPlaylist();
	}
	
	public void showCollection() {
		if(this.collection.getAlbums().isEmpty()) {
			message(1, "No album found.");
		}else {
			printCollection(this.collection.getAlbums());
		}
	}
	
	private void printCollection(ArrayList<Album> collection) {
		System.out.println("Collections");
		if(!collection.isEmpty()) {
			for(Album album: collection) {
				System.out.println("\t Album: "+album.getTitle());
				
				if(!album.getSongs().isEmpty()) {
					System.out.println("\t\tTitle \t\tDuration");
					printSong(album.getSongs());
				}else {
					message(1, "No song found.");
				}
			}			
		}else {
			message(1, "No collection found.");
		}
	}
	
	private void printSong(LinkedList<Song> list) {
		ListIterator<Song> songList = list.listIterator();
		if(songList.hasNext()) {
			while(songList.hasNext()) {
				Song item = songList.next();
				System.out.println("\t\t " + item.getTitle() + "\t\t" + item.getDuration());
			}			
		}else {
			message(1, "Empty list");
		}
	}

	private void message(int type, String msg) {
		String msgtype = "";
		
		switch(type) {
		case 1:
			msgtype = "Information: ";
			break;
		case 2:
			msgtype = "Warning: ";
			break;
		case 3:
			msgtype = "";
			break;
		}
		
		System.out.println(msgtype + msg);
	}
}
