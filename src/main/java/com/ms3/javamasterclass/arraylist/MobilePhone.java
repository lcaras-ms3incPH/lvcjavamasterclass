package com.ms3.javamasterclass.arraylist;

import java.util.ArrayList;

public class MobilePhone {
	private ArrayList<Contacts> phonebook = new ArrayList<Contacts>();
	
	public MobilePhone() {
		
	}
	
	public ArrayList<Contacts> getPhonebook(){
		return this.phonebook;
	}
	
	private void save(Contacts contact) {
		phonebook.add(contact);
	}
	
	private void edit(int index, Contacts contact) {
		this.phonebook.set(index, contact);
		System.out.println("Contact updated.\n" + contact.getName() + "(" + contact.getPhoneNumber() + ")");
	}
	
	public int getIndex(String value) {
		for(Contacts item: this.phonebook) {
			if(item.getName().equals(value) || item.getPhoneNumber().equals(value)) {
				return this.phonebook.indexOf(item);
			}
		}
		return -1;
	}
	
	private boolean contains(String name, String number) {
		for(Contacts item: this.phonebook) {
			if(item.getName().equals(name) || item.getPhoneNumber().equals(number)) {
				return true;
			}
		}
		return false;
	}
	
	private Contacts createContact(String name, String number) {
		return new Contacts(name, number);
	}
	
	public void newContact(String name, String number) {
		if(contains(name, number)) {
			System.out.println("Contact already exist.");
		}else {
			save(createContact(name, number));
			System.out.println("New contact is saved.");
		}
	}
	
	public void edit(int index, String name, String number) {
		edit(index, createContact(name, number));
	}
	
	public void remove(int index) {
		this.phonebook.remove(index);
	}
	
	public void showContacts() {
		System.out.println("=============================\nCONTACT\n-----------------------------");
		if(this.phonebook.isEmpty()) {
			System.out.println("No contacts.\n-----------------------------");
		}else {
			for(int i=0; i<phonebook.size(); i++) {
				System.out.println(
						(i+1) + ") Name:" + phonebook.get(i).getName() + 
						"\n\t" + phonebook.get(i).getPhoneNumber() +
						"\n-----------------------------");
				System.out.println();
			}
		}
	}
	
	public void showItem(int index) {
		System.out.println("Name: " + this.phonebook.get(index).getName() + "\nNumber: " + this.phonebook.get(index).getPhoneNumber());
	}
}
