package com.ms3.javamasterclass.arraylist;

import java.util.Scanner;

public class Main {
	private static Scanner input = new Scanner(System.in);
	private static MobilePhone mobile = new MobilePhone();
	
	public static void main(String[] args) {
		boolean quit = false;
		int select = 0;
		
		while(!quit) {
			printMenu();
			System.out.print("Select an option: ");
			select = input.nextInt();
			input.nextLine();
			
			switch(select) {
			case 0:
				printMenu();
				break;
			case 1:
				createContact();
				break;
			case 2:
				editContact();
				break;
			case 3:
				searchContact();
				break;
			case 4:
				deleteContact();
				break;
			case 5:
				showContacts();
				break;
			case 6:
				msg(2, "Thank you!");
				quit = true;
			}
		}
	}
	
	public static void printMenu() {
		System.out.println("Contacts:");
		System.out.println("\t[0] Print Menu");
		System.out.println("\t[1] Add Contact");
		System.out.println("\t[2] Edit Contact");
		System.out.println("\t[3] Search");
		System.out.println("\t[4] Delete");
		System.out.println("\t[5] Show Contacts");
		System.out.println("\t[6] quit");
	}
	
	public static void createContact() {
		String name = "", number = "";
		msg(3, "Enter name");
		name = input.nextLine();
		msg(3, "Enter number");
		number = input.nextLine();
		
		mobile.newContact(name, number);
	}
	
	public static void editContact() {
		msg(3, "Enter name or phone number");
		int index = mobile.getIndex(input.nextLine());
		String name = "", number = "";
		if(index>-1) {
			msg(3, "Enter new name: ");
			name = input.nextLine();
			msg(3, "Enter new number: ");
			number = input.nextLine();
			mobile.edit(index, name, number);
		}else {
			msg(1, "No contact found.");
		}
	}
	
	public static void searchContact() {
		msg(3, "Enter name or phone number.");
		int index = mobile.getIndex(input.nextLine());
		if(index>-1) {
			mobile.showItem(index);
		}else {
			msg(1, "No contact found.");
		}
	}
	
	public static void deleteContact() {
		msg(3, "Enter the name or number.");
		int index = mobile.getIndex(input.nextLine());
		if(index>-1) {
			mobile.remove(index);
			msg(2, "Contact deleted.");
		}else {
			msg(1, "No contact found!");
		}
	}
	
	public static void showContacts() {
		mobile.showContacts();
	}
	
	public static void msg(int type, String message) {
		String msgType = "";
		switch(type) {
		case 1:
			msgType = "Warning! ";
			break;
		case 2:
			msgType = "Information! ";
			break;
		case 3:
			msgType = "Input ";
			break;
		default:
			msgType = "";
		}
		
		System.out.println(msgType + " : " + message);
	}
	
	
}
