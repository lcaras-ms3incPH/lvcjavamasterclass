package com.ms3.javamasterclass.setschallenge;

import java.util.HashSet;
import java.util.Set;

public abstract class HeavenlyBody {

    private final Key key;
    private final double orbitalPeriod;
    private final Set<HeavenlyBody> satellites;
    
    public enum BodyTypes {
    	STAR,
    	PLANET,
    	DWARF_PLANET,
    	MOON,
    	COMET,
    	ASTEROID
    }
    
    public HeavenlyBody(String name, double orbitalPeriod, BodyTypes bodyTypes) {
        this.key = new Key(name, bodyTypes);
        this.orbitalPeriod = orbitalPeriod;
    	this.satellites = new HashSet<>();
    	
    }

	public double getOrbitalPeriod() {
		return orbitalPeriod;
	}

    public Key getKey() {
		return key;
	}
    
	public boolean addSatellite(HeavenlyBody moon){
        return this.satellites.add(moon);
    }

	public Set<HeavenlyBody> getSatellites() {
        return new HashSet<>(this.satellites);
    }

    @Override
    public final boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        
        if(obj instanceof HeavenlyBody) {
        	HeavenlyBody theObj = (HeavenlyBody) obj;
        	return this.key.equals(theObj.getKey());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.key.hashCode();
    }
    
    public static Key newKey(String name, BodyTypes bodyTypes) {
    	return new Key(name, bodyTypes);
    }

	@Override
	public String toString() {
		return this.key.name + ": " + this.key.bodyTypes + ", " + this.getOrbitalPeriod();
	}
    
	public static final class Key {
		private String name;
		private BodyTypes bodyTypes;
		
		public Key(String name, BodyTypes bodyTypes) {
			this.name = name;
			this.bodyTypes = bodyTypes;
		}

		public String getName() {
			return name;
		}

		public BodyTypes getBodyTypes() {
			return bodyTypes;
		}

		@Override
		public boolean equals(Object obj) {
			Key theKey = (Key) obj;
//			System.out.println("equals check: " + this.name.equals(theKey.getName()));
			if(this.name.equals(theKey.getName())) {
				return this.bodyTypes == theKey.getBodyTypes();
			}
			return false;
		}

		@Override
		public int hashCode() {
			return this.name.hashCode() + 57 + this.bodyTypes.hashCode();
		}

		@Override
		public String toString() {
			return this.name + ": " + this.bodyTypes;
		}
	}
}
