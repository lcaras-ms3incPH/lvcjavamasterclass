package com.ms3.javamasterclass.setschallenge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    private static Map<HeavenlyBody.Key, HeavenlyBody> solarSystem = new HashMap<>();
    private static Set<HeavenlyBody> planets = new HashSet<>();

    public static void main(String[] args) {
        HeavenlyBody temp = new Planet("Mercury", 88);
        solarSystem.put(temp.getKey(),temp);
        planets.add(temp);

        temp = new Planet("Pluto", 225);
        solarSystem.put(temp.getKey(),temp);
        planets.add(temp);

        temp = new Planet("Earth", 365);
        solarSystem.put(temp.getKey(),temp);
        planets.add(temp);

        HeavenlyBody tempMoon = new Moon("Moon",27);
        solarSystem.put(tempMoon.getKey(),tempMoon);
        temp.addSatellite(tempMoon);
        
        Set<HeavenlyBody> satellites = new HashSet<>();
        for(HeavenlyBody planet:planets) {
        	satellites.addAll(planet.getSatellites());
        }
        
        System.out.println("Planets are: ");
        for (HeavenlyBody planet:planets){
        	System.out.println("\t"+planet.getKey());
        }
        
        HeavenlyBody body = solarSystem.get(HeavenlyBody.newKey("Earth", HeavenlyBody.BodyTypes.PLANET));
        System.out.println("Moon of " + body.getKey());
        for(HeavenlyBody earthMoon: body.getSatellites()) {
        	System.out.println("\t" + earthMoon.getKey());
        }
        
        System.out.println("All satellites");
        for(HeavenlyBody satellite: satellites) {
        	System.out.println("\t" + satellite.getKey());
        }
        
        HeavenlyBody pluto = new Planet("Pluto",842);
        
        planets.add(pluto);
        
        for (HeavenlyBody planet: planets){
        	System.out.println(planet);
        }
        
        HeavenlyBody earth1 = new Planet("Earth", 365);
        HeavenlyBody earth2 = new Planet("Earth", 365);
        System.out.println(earth1.equals(earth2));
        System.out.println(earth2.equals(earth1));
        System.out.println(earth1.equals(pluto));
        System.out.println(pluto.equals(earth2));
        
        solarSystem.put(pluto.getKey(), pluto);
        System.out.println(solarSystem.get(HeavenlyBody.newKey("Pluto", HeavenlyBody.BodyTypes.PLANET)));
        System.out.println(solarSystem.get(HeavenlyBody.newKey("Pluto", HeavenlyBody.BodyTypes.DWARF_PLANET)));
    }
}

