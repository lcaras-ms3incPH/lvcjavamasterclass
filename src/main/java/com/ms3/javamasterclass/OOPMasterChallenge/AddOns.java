package com.ms3.javamasterclass.OOPMasterChallenge;

public class AddOns {
	private Lettuce lettuce;
	private Bacon bacon;
	private Carrot carrot;
	private Cheese cheese;
	private Onion onion;
	private Tomato tomato;
	
	private double price;

	public AddOns() {
		//regular addons
		this.lettuce = new Lettuce();
		this.bacon = new Bacon();
		this.carrot = new Carrot();
		this.cheese = new Cheese();
		this.onion = new Onion();
		this.tomato = new Tomato();			
		
		this.price = 0.0;
	}

	public String addonList() {
		this.price = 0.0;
		String addonList = "";
		int cnt = 0;
		
		if(this.lettuce.isAdded()) {
			cnt++;
			addonList+=validate(this.lettuce, cnt);
		}
		
		if(this.bacon.isAdded()) {
			cnt++;
			addonList+=validate(this.bacon, cnt);
		}

		if(this.carrot.isAdded()) {
			cnt++;
			addonList+=validate(this.carrot, cnt);
		}

		if(this.cheese.isAdded()) {
			cnt++;
			addonList+=validate(this.cheese, cnt);
		}

		if(this.onion.isAdded()) {
			cnt++;
			addonList+=validate(this.onion, cnt);
		}
		
		if(this.tomato.isAdded()) {
			cnt++;
			addonList+=validate(this.tomato, cnt);
		}
		
		if(addonList.isEmpty())
			return "No addon";
		
		return addonList;
	}

	private String validate(Ingredient ing, int cnt) {
		if(cnt>4)
			return "\nReached the limit. You can only get 4 addons\n";
		else
			return addList(ing);
	}
	
	private String addList(Ingredient ing) {
		this.price+=ing.getPrice();
		return ing.getName() + " = " + ing.getPrice() + "\n";
	}
	
	public Lettuce getLettuce() {
		return lettuce;
	}

	public Bacon getBacon() {
		return bacon;
	}

	public Carrot getCarrot() {
		return carrot;
	}

	public Cheese getCheese() {
		return cheese;
	}

	public Onion getOnion() {
		return onion;
	}

	public Tomato getTomato() {
		return tomato;
	}

	public double getPrice() {
		return price;
	}
}
