package com.ms3.javamasterclass.OOPMasterChallenge;

public class Deluxe extends Hamburger{
	private Softdrink soda;
	private Chips chipy;
	
	public Deluxe() {
		super("Sesame Seed Bun", 1, 5.0);
		soda = new Softdrink();
		chipy = new Chips();
	}

	@Override
	public void showBurger() {
		System.out.println(getClass().getSimpleName() + " Hamburger (" + 
							this.soda.getName() + " + " + this.chipy.getName() + ")");
		System.out.println("Roll/Bread: " + this.getBread());
		System.out.println("Meat layers: " + this.getMeat());
		System.out.println("Addons: \n" + this.getAddons().addonList());
		System.out.println("Additional Price: " + this.getAddOns().getPrice());
		System.out.println("Base Price: " + this.getBasePrice());
		System.out.println("Total Price: " + (this.getTotalPrice()+this.getAddOns().getPrice()));
	}
	
}
