package com.ms3.javamasterclass.OOPMasterChallenge;

public class Bundle {
	private String name;
	private boolean isAdded;
	
	public Bundle(String name) {
		this.name = name;
		this.isAdded = true;
	}

	public String getName() {
		return name;
	}
}
