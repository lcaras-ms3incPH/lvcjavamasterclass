package com.ms3.javamasterclass.OOPMasterChallenge;

public class Healthy extends Hamburger {
	private HealthyAddon healthyAddon;
	public Healthy() {
		super("Brown Rye Bread", 1, 5.0);
		this.healthyAddon = new HealthyAddon();
	}
	
	@Override
	public void showBurger() {
		System.out.println(getClass().getSimpleName() + " Hamburger");
		System.out.println("Roll/Bread: " + this.getBread());
		System.out.println("Meat layers: " + this.getMeat());
		System.out.println("Addons: \n" + this.getAddons().addonList());

		System.out.println("Healthy Addons: \n" +
							this.healthyAddon.addonList());
		
		System.out.println("Additional Price: " + 
							(this.getAddons().getPrice() +
									this.healthyAddon.getPrice()));
		System.out.println("Base Price: " + this.getBasePrice());
		System.out.println("Total Price: " + 
							(this.getTotalPrice()+
									this.getAddOns().getPrice()+
									this.healthyAddon.getPrice()));
	}

	public HealthyAddon getHealthyAddon() {
		return healthyAddon;
	}
}
