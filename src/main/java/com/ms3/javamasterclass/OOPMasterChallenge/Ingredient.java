package com.ms3.javamasterclass.OOPMasterChallenge;

public class Ingredient {
	private String name;
	private double price;
	private boolean isAdded;
	
	public Ingredient(String name, double price) {
		this.name = name;
		this.price = price;
		this.isAdded = false;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}
	
	public boolean isAdded() {
		return isAdded;
	}

	public void add() {
		if(!this.isAdded)
			this.isAdded = true;
		else
			System.out.println("To much "+this.getClass().getSimpleName()+". Not allowed.");
	}
	
	public void remove() {
		this.isAdded = false;
	}
}
