package com.ms3.javamasterclass.OOPMasterChallenge;

public class Carrot extends Ingredient {
	
	public Carrot() {
		super("Carrot", 0.2);
	}
	
}
