package com.ms3.javamasterclass.OOPMasterChallenge;

public class HealthyAddon {
	private Cucumber cucumber;
	private Egg egg;
	private Mushroom mushroom;
	
	private double price;

	public HealthyAddon() {
		//heathy addons
		this.cucumber = new Cucumber();
		this.egg = new Egg();
		this.mushroom = new Mushroom();
		
		this.price = 0.0;
	}

	public String addonList() {
		int cnt = 0;
		this.price = 0.0;
		String addonList = "";

		if(this.cucumber.isAdded()) {
			cnt++;
			addonList+=validate(this.cucumber, cnt);
		}
		
		if(this.egg.isAdded()) {
			cnt++;
			addonList+=validate(this.egg, cnt);
		}

		if(this.mushroom.isAdded()) {
			cnt++;
			addonList+=validate(this.mushroom, cnt);
		}
		
		if(addonList.isEmpty())
			return "No addon";
		
		return addonList;
	}
	
	private String validate(Ingredient ing, int cnt) {
		if(cnt>2)
			return "\nReached the limit. You can only get 2 health addons\n";
		else
			return addList(ing);
	}
	
	private String addList(Ingredient ing) {
		this.price+=ing.getPrice();
		return ing.getName() + " = " + ing.getPrice() + "\n";
	}
	
	public Cucumber getCucumber() {
		return cucumber;
	}

	public Egg getEgg() {
		return egg;
	}

	public Mushroom getMushroom() {
		return mushroom;
	}

	public double getPrice() {
		return price;
	}
}


