package com.ms3.javamasterclass.OOPMasterChallenge;

public class Tomato extends Ingredient {

	public Tomato() {
		super("Tomato", 0.2);
	}
	
}
