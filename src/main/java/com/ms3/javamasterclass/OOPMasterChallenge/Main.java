package com.ms3.javamasterclass.OOPMasterChallenge;

public class Main {
	public static void main(String[] args) {
		Hamburger ham = new Hamburger("Regular", 1, 3.0);
		
		ham.getAddons().getLettuce().add();
		ham.getAddons().getBacon().add();
		ham.getAddons().getCarrot().add();
		ham.getAddons().getCheese().add();
		ham.getAddons().getOnion().add();
		
		ham.showBurger();
		
		System.out.println("=============");
		
		Healthy health = new Healthy();
		health.getAddons().getBacon().add();
		health.getAddons().getCarrot().add();
		health.getAddons().getCheese().add();
		health.getAddons().getLettuce().add();
		health.getHealthyAddon().getEgg().add();
		health.getHealthyAddon().getMushroom().add();
		health.getHealthyAddon().getCucumber().add();
		health.showBurger();
		
		System.out.println("=============");
		
		Deluxe deluxe = new Deluxe();
		deluxe.getAddOns().getBacon().add();
		deluxe.getAddons().getCarrot().add();
		deluxe.getAddOns().getLettuce().add();
		deluxe.getAddons().getCheese().add();
		deluxe.getAddOns().getOnion().add();
		deluxe.showBurger();
	}
}
