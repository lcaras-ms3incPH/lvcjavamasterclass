package com.ms3.javamasterclass.OOPMasterChallenge;

public class Onion extends Ingredient {
	public Onion() {
		super("Onion", 0.2);
	}
}
