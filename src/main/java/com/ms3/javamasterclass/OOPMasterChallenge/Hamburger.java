package com.ms3.javamasterclass.OOPMasterChallenge;

public class Hamburger {
	private String bread;
	private int meat;
	private double basePrice;
	private double totalPrice;
	private double addonPrice;
	private AddOns addOns;
	
	public Hamburger(String bread, int meat, double basePrice) {
		this.bread = bread;
		this.meat = meat;
		this.basePrice = basePrice;
		this.totalPrice = basePrice;
		this.addonPrice = 0.0;
		this.addOns = new AddOns();
	}
	
	public void showBurger() {
		System.out.println(getClass().getSimpleName());
		System.out.println("Roll/Bread: " + this.bread);
		System.out.println("Meat layers: " + this.meat);
		System.out.println("Addons: \n" + this.getAddons().addonList());
		System.out.println("Additional Price: " + this.addOns.getPrice());
		System.out.println("Base Price: " + this.getBasePrice());
		System.out.println("Total Price: " + (this.totalPrice+this.addOns.getPrice()));
	}
	
	public AddOns getAddons() {
		return this.addOns;
	}

	public String getBread() {
		return bread;
	}

	public int getMeat() {
		return meat;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public AddOns getAddOns() {
		return addOns;
	}
	
	
}
