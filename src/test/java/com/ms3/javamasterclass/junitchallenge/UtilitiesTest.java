package com.ms3.javamasterclass.junitchallenge;

import static org.junit.Assert.*;

public class UtilitiesTest {
	
	private Utilities util;
	
	@org.junit.Before
	public void setup() {
		util = new Utilities();
	}

	@org.junit.Test
	public void everyNthChar() throws Exception {
		assertArrayEquals("el".toCharArray(), util.everyNthChar("hello".toCharArray(), 2));
	}
	
	@org.junit.Test
	public void everyNthCharGreaterThanArray() throws Exception {
		assertArrayEquals("hello".toCharArray(), util.everyNthChar("hello".toCharArray(), 6));
	}
	
	@org.junit.Test
	public void removePairs() throws Exception {
		assertEquals("ABCDEF", util.removePairs("AABCDDEFF"));
		assertEquals("ABCABDEF", util.removePairs("ABCCABDEEF"));
	}
	
	@org.junit.Test
	public void removePairsIgnoreCase() throws Exception {
		assertEquals("ABCDEF", util.removePairs("AaBCdDEFf"));
	}
	
	@org.junit.Test
	public void removePairsWithTwoChar() throws Exception {
		assertEquals("A", util.removePairs("Aa"));
	}

	@org.junit.Test
	public void removePairsNullValue() throws Exception {
		assertNull("Null entry!", util.removePairs(null));
	}

	@org.junit.Test
	public void converter() throws Exception {
		assertEquals(300, util.converter(10, 5));
	}

	@org.junit.Test(expected = ArithmeticException.class)
	public void converterArithmeticExc() throws Exception {
		assertEquals(300, util.converter(10, 0));
	}

	@org.junit.Test
	public void nullIfOddLenght() throws Exception {
		assertEquals(null, util.nullIfOddLenght("hello"));
	}

	@org.junit.Test
	public void nullIfOddLenghtEvenValue() throws Exception {
		assertEquals("Lemuel", util.nullIfOddLenght("Lemuel"));
	}
}
